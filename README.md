# README #

This is a project for COE768 (Computer Networks) - Fall/2014. The project consists of a P2P file transfer system where there is a central server listens for peers (clients) to connect and register files. Once a peer has registered a file, another peer can request a list of registered files from the server, and subsequently request a file from the peer who registered it.
The project was implemented in C to run in Linux systems, and it incorporates concepts of socket programming together with multithreaded programming, while avoiding common issues in multithreading with the use of a mutex.

Authors: Ionathan Zauritz - izauritz@ryerson.ca, Yuming Guo - y25guo@ryerson.ca
/* server.c	 - compile with -lpthread 
 COE768 P2P project  by Ionathan Zauritz and Yuming Guo */
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <strings.h>


#define SERVER_TCP_PORT 18000	/* default port */
#define BUFLEN		101	/* buffer length */
#define MAX_CONT 100 //max # of contents
#define MAX_PEERS 10 //max # of peers

void *connection(void *sdd);
void clearContent();
void reaper(int);

pthread_mutex_t mutex;// = PTHREAD_MUTEX_INITIALIZER;

char content[MAX_CONT][3][20]; //list of content 0-content name, 1-address, 2-peer name. 20 char each.
int sdIndex[MAX_CONT]; //stores sd for connections


int main(int argc, char **argv)
{
	int 	sd, new_sd, client_len, port;
	struct	sockaddr_in server, client;

	switch(argc){
	case 1:
		port = SERVER_TCP_PORT;
		break;
	case 2:
		port = atoi(argv[1]);
		break;
	default:
		fprintf(stderr, "Usage: %d [port]\n", argv[0]);
		exit(1);
	}

	/* Create a stream socket	*/
	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "Can't creat a socket\n");
		exit(1);
	}

	/* Bind an address to the socket	*/
	bzero((char *)&server, sizeof(struct sockaddr_in));
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&server, sizeof(server)) == -1){
		fprintf(stderr, "Can't bind name to socket\n");
		exit(1);
	}

	/* queue up to MAX_PEERS connect requests  */
	listen(sd, MAX_PEERS);

	int iret[MAX_PEERS];
    pthread_t thread[MAX_PEERS];

	(void) signal(SIGCHLD, reaper);

	clearContent(); //this clears content list

	int i = 0;
	while(1) {
        if(i>=MAX_PEERS){break;}
        client_len = sizeof(client);

        new_sd = accept(sd, (struct sockaddr *)&client, &client_len);

        if(new_sd < 0){
        fprintf(stderr, "Can't accept client \n");
        exit(1);
        }

        //this fixes segmentation fault caused by passing "(void*) new_sd" to thread
        int* new_sdp = malloc( sizeof( *new_sdp )) ;
        *new_sdp = new_sd ;

        //create thread
        iret[i] = pthread_create(&thread[i], NULL, connection, new_sdp);
        if(iret[i] != 0){
            printf("thread[%d] not generated!\n", i);
        }
        i++;
	}
}


void *connection(void *sdd)
{
    int sd =* (int *) sdd;
	char	*bp, buf[BUFLEN];
	int 	i, n, bytes_to_read;
	char type; //stores PDU type
	char data[BUFLEN-1]; //stores data

    while(1){
        //clear buffer
        for(i=0; i<BUFLEN; i++){
            buf[i] = '\0';
        }

        n = read(sd, buf, BUFLEN); //receive request
        if(n <= 0){break;} //exits loop if n == 0(disconnect), or n < 0(error)
        type = buf[0]; //save PDU type requested
        for(i=0; i<BUFLEN-1; i++){ //save data
            data[i] = buf[i+1];
        }

        // options
        //Content Registration
		pthread_mutex_lock(&mutex);
        if(type == 'R' || type == 'r'){
            char tmpName[20];
            int z = 0;
            for(i=0; i<20; i++){
                tmpName[i] = data[i+20];
            }
            for(i=0; i<MAX_CONT; i++){
                if(strcmp(tmpName, content[i][0]) == 0){
                    z =1;
                }
            }
            if(z == 0){
                int x = -1; //for content index available
                for(i=0; i<MAX_CONT; i++){ //find content index available
                    if(content[i][0][0] == '\0'){
                        x = i;
                        break;
                    }
                }
                for(i=0; i<20; i++){ //load content list
                    if(x == -1){break;}//if list is full, exit loop
                    content[x][2][i] = data[i];	//name of peer
                    content[x][0][i] = data[i+20]; //name of content
                    content[x][1][i] = data[i+40]; //address
                }
                sdIndex[x] = sd;
                //A/E
                if(x == -1){ //content list is full
                    printf("Content list is full!\n");
                    write(sd, "EContent list is full!", 23); //send E
                }
                else{ //if no error
                    write(sd, "A", 2); //send A
                }
            }
            else{
                printf("A file with this name is already registered!\n");
                write(sd, "EA file with this name is already registered!", 46); //send E
            }
        }
        //Content Download
        else if(type == 'D' || type == 'd'){
            printf("Server doesn't send files!\n");
            continue; //jump to start of loop
        }
        //Search Content Server
        else if(type == 'S' || type == 's'){
            int ind = -1; //stores index
            for(i=0; i<MAX_CONT; i++){ //to find index
                if(strcmp(content[i][0], data) == 0){
                    ind = i;
                    break;
                }
            }
            if(ind == -1){ //not found
                printf("File requested not in list!\n");
                write(sd, "EFile requested not in list!", 29); //send E
            }
            else{
                //clear buffer
                for(i=0; i<BUFLEN; i++){
                    buf[i] = '\0';
                }
                buf[0] = 'S';
                strcat(buf, content[ind][1]);
                write(sd, buf, BUFLEN);
            }
        }
        //Content De-Registration
        else if(type == 'T' || type == 't'){
            for(i=0; i<MAX_CONT; i++){
                if(strcmp(data,content[i][0]) == 0){
                    content[i][0][0] = '\0';
                    sdIndex[i] = 0;
                    break;
                }
            }
        }
        //Content Data
        else if(type == 'C' || type == 'c'){
            printf("Server doesn't send data!\n");
            continue; //jump to start of loop
        }
        //List of On-Line Registration Content
        else if(type == 'O' || type == 'o'){
            for(i=0; i<BUFLEN; i++){
                buf[i] = '\0';
            }
            buf[0] = 'O';
            for(i=0; i<MAX_CONT; i++){
                if(content[i][0][0] == '\0'){continue;} //skips if empty
                strcat(buf, content[i][0]);
                strcat(buf, "\tby: ");
                strcat(buf, content[i][2]);
                if(i >= MAX_CONT-1){break;} // to skip the last '\n'
                strcat(buf,"\n");
            }
            write(sd, buf, BUFLEN);

        }
        //Acknowledgement
        else if(type == 'A' || type == 'a'){
            printf("Server should not receive acknowledgement!\n");
        }
        //Error
        else if(type == 'E' || type == 'e'){
            printf("Error received!\n");
        }
        else{
            printf("Invalid PDU type!\n");
            write(sd, "EInvalid PDU type!", 19); //send E

        }
		pthread_mutex_unlock(&mutex);
    } // loop ends here

	pthread_mutex_lock(&mutex);
    close(sd);
    // this deletes from list every index that that was registered by this sd
    for(i=0; i<MAX_CONT; i++){
        if(sdIndex[i] == sd){
            content[i][0][0] = '\0';
            sdIndex[i] = 0;
        }
    }
	pthread_mutex_unlock(&mutex);
}

void clearContent(){
    int i;
    for(i=0; i<MAX_CONT; i++){
        content[i][0][0] = '\0';
    }
}

/*	reaper		*/
void	reaper(int sig)
{
	int	status;
	while(wait3(&status, WNOHANG, (struct rusage *)0) >= 0);
}

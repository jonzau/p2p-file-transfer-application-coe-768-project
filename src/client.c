/* client.c 
 COE768 P2P project  by Ionathan Zauritz and Yuming Guo */
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/signal.h>
#include <stdlib.h>
#include <strings.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <unistd.h>

#define SERVER_TCP_PORT 18000	/* default port */
#define BUFLEN		101	/* buffer length */
#define MAX_PEERS 15 //max # of peers to download at the same time.

int clientToServer(int sd);
int download(char contName[], char ip[]);
int upload(int sd);
int getIp();
int p2pServer();
void r(char s[]);
void reaper(int sig);

char name[20];
char localIP[NI_MAXHOST];

int main(int argc, char **argv)
{
	int 	n, i, bytes_to_read;
	int 	sd, port;
	struct	hostent		*hp;
	struct	sockaddr_in server;
	char	*host, *bp, rbuf[BUFLEN], sbuf[BUFLEN];

    switch(argc){
    case 2:
        host = argv[1];
        port = SERVER_TCP_PORT;
        break;
    case 3:
        host = argv[1];
        port = atoi(argv[2]);
        break;
    default:
        fprintf(stderr, "Usage: %s host [port]\n", argv[0]);
        exit(1);
    }

    /* Create a stream socket	*/
    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "Can't creat a socket\n");
        exit(1);
    }

    bzero((char *)&server, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    if (hp = gethostbyname(host))
      bcopy(hp->h_addr, (char *)&server.sin_addr, hp->h_length);
    else if ( inet_aton(host, (struct in_addr *) &server.sin_addr) ){
      fprintf(stderr, "Can't get server's address\n");
      exit(1);
    }

    /* Connecting to the server */
    if (connect(sd, (struct sockaddr *)&server, sizeof(server)) == -1){
      fprintf(stderr, "Can't connect \n");
      exit(1);
    }

	printf("Enter name: "); //as user for name
	r(name); //reads name
	for(i=0; i<20; i++){
        if(name[i] == '\n'){
            name[i] = '\0';
            break;
        }
    }

	int pid = fork();
	if(pid == 0){ //child
        p2pServer();
	}
	else{ //parent
        usleep(250000); //wait 0.25 seconds for p2p to initialize
        clientToServer(sd);
        close(sd);
	}

	return(0);
}

int clientToServer(int sd)//function for client to server
{
    char sbuf[BUFLEN];
    char option, opStr[20];
    char contName[20];
    int n, i;

    while(1){
        printf("\n\t'R' to register file.\n\t'T' to deregister.\n\t'D' to download.\n\t'O' to list files available.\n\t'Q' to exit.\n\n\t>");
		do{
            r(opStr);
			option = opStr[0];
		} while (option == '\n' || option == '\0');
        if(option == 'R' || option == 'r'){
			do{
                printf("Enter file name to register: ");
                r(contName);
			} while(contName[0] == '\n' || contName[0] == '\0');

            for(i=0; i<20; i++){
                if(contName[i] == '\n'){
                    contName[i] = '\0';
                }
            }
            //check if file exists
			FILE	*p;
            p=fopen(contName, "r");
            if(p==NULL){
                printf("File '%s' doesnt exist!\n", contName);
                fclose(p);
                continue;
            }
            fclose(p);

            getIp(); //gets local IP to send to server

            //clear buffer
            for(i=0; i<BUFLEN; i++){
                sbuf[i] = '\0';
            }
            sbuf[0] = 'R';
            for(i=1; i<21; i++){
                sbuf[i] = name[i-1];
                sbuf[i+20] = contName[i-1];
                sbuf[i+40] = localIP[i-1];
            }
            write(sd, sbuf, BUFLEN);		/* send it out */
            read(sd,sbuf, BUFLEN);

            if(sbuf[0] == 'A'){printf("File '%s' registered.\n", contName);}
            else if(sbuf[0] == 'E'){
                char msg[BUFLEN-1];
                for(i=0; i<BUFLEN-1; i++){
                    msg[i] = sbuf[i+1];
                }
                printf("Error! File '%s' not registered.\n%s\n", contName, msg);
            }
        }
        else if(option == 'T' || option == 't'){
            do{
                printf("Enter file name to de-register: ");
                r(contName);
			} while(contName[0] == '\n' || contName[0] == '\0');

            for(i=0; i<20; i++){
                if(contName[i] == '\n'){
                    contName[i] = '\0';
                    break;
                }
            }
            //clear buffer
            for(i=0; i<BUFLEN; i++){
                sbuf[i] = '\0';
            }
            sbuf[0] = 'T';
            for(i=1; i<21; i++){
                sbuf[i] = contName[i-1];
            }
            write(sd, sbuf, BUFLEN);		/* send it out */
        }
        else if(option == 'D' || option == 'd'){
            do{
                printf("Enter file name to download: ");
                r(contName);
			} while(contName[0] == '\n' || contName[0] == '\0');

            for(i=0; i<20; i++){
                if(contName[i] == '\n'){
                    contName[i] = '\0';
                    break;
                }
            }
            //clear buffer
            for(i=0; i<BUFLEN; i++){
                sbuf[i] = '\0';
            }
            sbuf[0] = 'S';
            for(i=0; i<20; i++){
                sbuf[i+1] = contName[i];
            }
            write(sd, sbuf, BUFLEN);		/* send it out */
            read(sd,sbuf, BUFLEN);

            if(sbuf[0] == 'S'){
                char peerIp[20];
                for(i=0; i<20; i++){
                    peerIp[i] = sbuf[i+1];
                }
                download(contName, peerIp);
            }
            else if(sbuf[0] == 'E'){
                printf("File unavailable!\n");
            }
            else{
                printf("Unknown error!");
            }

        }
        else if(option == 'O' || option == 'o'){
            write(sd, "O", 2);		/* send it out */
            read(sd,sbuf, BUFLEN);
            if(sbuf[0] == 'O'){
                char list[BUFLEN-1];
                for(i=0; i<BUFLEN-1; i++){
                    list[i] = sbuf[i+1];
                }
                printf("\nList:\n%s\n", list);
            }
            else{printf("Error retriving list!");}
        }
        else if(option == 'Q' || option == 'q'){
            close(sd);
            return 0;
        }
        else{
            printf("Invalid request!\n");
        }
    }
    close(sd);
    return 0;
}

int download(char contName[], char ip[]){
    int 	n, i, bytes_to_read;
	int 	sd, port;
	struct	hostent		*hp;
	struct	sockaddr_in server;
	char	*host, *bp, rbuf[BUFLEN], sbuf[BUFLEN];


	host = ip;
    port = SERVER_TCP_PORT; //atoi(argv[2]);


	/* Create a stream socket	*/
	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "Can't creat a socket\n");
		exit(1);
	}

	bzero((char *)&server, sizeof(struct sockaddr_in));
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	if (hp = gethostbyname(host))
	  bcopy(hp->h_addr, (char *)&server.sin_addr, hp->h_length);
	else if ( inet_aton(host, (struct in_addr *) &server.sin_addr) ){
	  fprintf(stderr, "Can't get server's address\n");
	  exit(1);
	}

	/* Connecting to the server */
	if (connect(sd, (struct sockaddr *)&server, sizeof(server)) == -1){
	  fprintf(stderr, "Can't connect \n");
	  exit(1);
	}

    //clear buffer
    for(i=0; i<BUFLEN; i++){
		sbuf[i] = '\0';
	}
	sbuf[0] = 'D';
	strcat(sbuf, contName);
	write(sd, sbuf, BUFLEN);	// send file name	/* send it out */

	FILE *pk;
	int k;
	pk=fopen(contName,"w");
	if(pk==NULL)
	{
		printf("error openning file");
		return 1;
	}
    //clear buffer
    for(i=0; i<BUFLEN; i++){
        sbuf[i] = '\0';
    }
    int rData = 0;
	while(n=read(sd, sbuf, BUFLEN)){
        if(sbuf[0] != 'C'){
            printf("Download failed!\nData received contains incorrect PDU type!\n");
            fclose(pk);
            close(sd);
            return(1);
        }
        printf("%d B of data received\n",strlen(sbuf));
        for(i=1; i<strlen(sbuf); i++){
            fprintf(pk, "%c", sbuf[i]);;
        }
        //clear buffer
        for(i=0; i<BUFLEN; i++){
            sbuf[i] = '\0';
        }
	}
	if(rData > 0){
        printf("%d B of data received\n",rData);
	}
	printf("File '%s' downloaded.\n", contName);
	fclose(pk);

	close(sd);
	return 0;

}
int p2pServer(){
    printf("Initializing P2P server.\n");
    int 	sd, new_sd, client_len, port;
	struct	sockaddr_in server, client;

	port = SERVER_TCP_PORT;

	/* Create a stream socket	*/
	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "p2p server can't creat a socket\n");
		exit(1);
	}

	/* Bind an address to the socket	*/
	bzero((char *)&server, sizeof(struct sockaddr_in));
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&server, sizeof(server)) == -1){
		fprintf(stderr, "p2p server can't bind name to socket\n");
		exit(1);
	}

	/* queue up to MAX_PEERS connect requests  */
	listen(sd, MAX_PEERS);

	(void) signal(SIGCHLD, reaper);
    printf("P2P server ready.\n");
	while(1) {
	  client_len = sizeof(client);
	  new_sd = accept(sd, (struct sockaddr *)&client, &client_len);
	  if(new_sd < 0){
	    fprintf(stderr, "p2p server can't accept client \n");
	    exit(1);
	  }
	  switch (fork()){
	  case 0:		/* child */
		(void) close(sd);
		exit(upload(new_sd));
	  default:		/* parent */
		(void) close(new_sd);
		break;
	  case -1:
		fprintf(stderr, "fork: error in p2p server.\n");
	  }
	}
}

/*	upload - sends file*/
int upload(int sd)
{
	char	*bp, buf[BUFLEN];
	int 	n, bytes_to_read;

	//clear buffer
	int i;
	for(i=0; i<BUFLEN; i++){
		buf[i] = '\0';
	}

	n = read(sd, buf, BUFLEN);	//buf is name of file

    if(buf[0] != 'D'){
        write(sd, "EInvalid PDU type!", 19); //send E
        printf("Invalid PDU type!\n");
        return 1;
    }

	char fileName[20];

	for(i=0; i<20; i++){
		fileName[i] = buf[i+1];
	}

    FILE	*t;
    t=fopen(fileName, "r");

    if(t==NULL){
        printf("file open error");
        write(sd, "EFile not found!\n", BUFLEN);
        fclose(t);
        return 1;
    }

    char tmp = '\n';
    unsigned char data[BUFLEN-1]={0};
    int nread = -1;
    while(nread != 0){
    nread = 0;
        //clear buffer
        for(i=0; i<BUFLEN-1; i++){
            buf[i+1] = '\0';
            data[i] = '\0';
        }
        buf[0] = 'C';
        //scan 100B from file
        nread = fread(data,1,BUFLEN-2,t);
        strcat(buf,data);
        data[nread] = '\n';
        for(i=0; i<nread; i++){
            buf[i+1] = data[i];
        }
        if(nread < 1){break;}
        write(sd, buf, BUFLEN);
        printf("\n%d B of data sent", nread+1);
    }

    fclose(t);
	close(sd);

    printf("\n\n\t>");

	return(0);
}

int getIp(){
   struct ifaddrs *ifaddr, *ifa;
   int family, s;
   char host[NI_MAXHOST];

   if (getifaddrs(&ifaddr) == -1) {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
   }

   for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        family = ifa->ifa_addr->sa_family;

        if (family == AF_INET) {
                s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in),
                                               host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
                if (s != 0) {
                        printf("getnameinfo() failed: %s\n", gai_strerror(s));
                        exit(EXIT_FAILURE);
                }
               // printf("<Interface>: %s \t <Address> %s\n", ifa->ifa_name, host); //this prints local ip addresses
        }
   }

    int i;
    for (i=0; i<NI_MAXHOST; i++){
        localIP[i] = host[i];
    }

    return 0;
}

/* I created this function because I was having problems with the input buffer */
void r(char s[]){
    int s_size = 20;
    char tmp = '\0';
    int i = 0;
    while(tmp != '\n'){
        tmp = getchar();
        s[i] = tmp;
        if(s[i] == '\n' || i >= s_size-1){
            s[i] = '\0';
            return;
        }
        i++;
    }
}

/*	reaper		*/
void	reaper(int sig)
{
	int	status;
	while(wait3(&status, WNOHANG, (struct rusage *)0) >= 0);
}
